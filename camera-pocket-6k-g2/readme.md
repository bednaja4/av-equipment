# Blackmagic Design Pocket Camera 6k G2
Blackmagic feature set [https://www.blackmagicdesign.com/products/blackmagicpocketcinemacamera](https://www.blackmagicdesign.com/products/blackmagicpocketcinemacamera)


## Equipment
1. Blackmagic Design Pocket Cinema Camera 6k G2 - [web](https://www.blackmagicdesign.com/products/blackmagicpocketcinemacamera/techspecs/W-CIN-19) - [manual](./manuals/Pocket_6k_G2-manual.pdf)
2. Sigma 24-70mm F2.8 DG OS HSM ART EF-mount - [web](https://www.sigmaphoto.com/24-70mm-f2-8-dg-os-hsm-a) - [manual](./manuals/Sigma-24_70mm_F28_DG_OS_HSM_A017.pdf)
3. Sigma 18-35mm F1.8 DC HSM ART EF-mount - [web](https://www.sigmaphoto.com/18-35mm-f1-8-dc-hsm-a-1) - [manual](./manuals/Sigma-18_35mm_F18_DC_HSM_A013.pdf)
4. Tilta Basic Kit for BMPCC 6K Pro/G2 - [web](https://tilta.com/shop/basic-kit-for-bmpcc-6k-pro/) - [manual](./manuals/Tilta-cage-manual.pdf)
5. Tilta Universal SSD Drive Holder Type I Black - [web](https://tilta.com/shop/tilta-universal-ssd-drive-holder-type-i-black/)

## Packing List
1. SSD Drive holder 
	- Disk holder
	- Right-Angled USB-CxUSB-C cable with screw lock
2. Sigma 18-35
3. Sigma 24-70
4. 5x NP-F battery (4 in case, 5th in camera)
5. DC power adapter for BMPCC 6k
6. Cage accesories from Tilta:
	- Top handle
	- 15mm rod holder
	- Battery grip security attachment
7. Pocket cinema camera 6k G2 in camera cage
8. Misc:
	- NP-F series battery USB charger
	- USB cable for the charger
	- HDMI 90-Degree adapter for camera cage
	- ThinFlex HDMI 

### Packing Photo
![Packing image](pocket6k-case.jpg)

# AV-equipment

List of equipment and packing list

## Photocameras
- Canon EOS M50 - [website](https://www.canon.cz/cameras/eos-m50/)
    - Canon EF-M 15-45mm F/3.5-6.3 IS STM
    - Canon EF-M 22 F/2 STM
    - EF EOS M adapter
    - Canon EF 85mm F/1.2 L II USM
    - Canon EF 200mm F/2.8 L II USM
- BMD [Pocket Cinema Camera 6K G2](/camera-pocket-6k-g2/)
    - Sigma 24-70 F2.8 DG OS HSM ART EF-mount 
    - Sigma 18-35 F1.8 DC HSM ART EF-mount
- Canon EOS 650D (Honza)
    - Samyang 24mm F/3.5 Tilt-Shift
    - Canon 70-200mm F/2.8L IS II
    - Canon 10-18
    - Canon 300mm F/4 L IS
- 2x Nikon D5100
    - Nikkor 55-300mm F/4.5-5.6
    - Nikkor 18-55 mm F/3.5-5.6
    - Tamron 60mm F/2 Macro 1:1
    - Sigma 10mm F2,8 HSM
- Nikon D700
    - Nikkor 24-70 F/2,8

## Package & List of equipment
- [Pocket Cinema Camera 6K G2](/camera-pocket-6k-g2/)
- [Slider and Pan-Tilt camera head](/camera-slider/)
- [Camera jib](/camera-jib/)
- [Freefly Wave camera](/freefly-wave/)
# Camera jib and Pan module
Edelkrone feature guide: [https://edelkrone.com/en-eu/pages/edelkrone-system-wide-features-guide](https://edelkrone.com/en-eu/pages/edelkrone-system-wide-features-guide)

## Equipment
1. Edelkrone JibONE - [web](https://edelkrone.com/en-eu/products/jibone) - [video manual](https://youtube.com/playlist?list=PLbDNm-U3aPICYN0BGRtNR4TUgiF-XlGLT)
2. Edelkrone Pan Module for JibONE - [web](https://edelkrone.com/en-eu/products/pan-module)
3. Edelkrone FlexTILT PRO - [web](https://edelkrone.com/products/flextilt?variant=40340536983634#pro)
4. Edelkrone USB Adapter - [web](https://edelkrone.com/products/edelkrone-link-adapter) - [video manual](https://youtube.com/playlist?list=PLbDNm-U3aPIDMP4m0vsczlAcidtIRUf31)
5. Edelkrone Wired Signal Hub for edelkrone Moco - [web](https://edelkrone.com/products/edelkrone-link-hub)

## Packing List
1. Power module
2. USB Adapter
3. Power sources:
	- 2x DC power adapter 230V-DC jack
4. Misc:
	- Wired signal hub
	- NP-F battery bracket for JibONE
	-
	-
5. JibONE
6. First counterbalance weight
7. 3x Counterbalance weight
8. Pan Module
9. FlexTILT PRO
10. Flat head 100mm for camera tripod

### Packing Photo
![Packing image](jib.jpg)
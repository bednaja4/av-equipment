# Camera slider and PT head
Edelkrone feature guide: [https://edelkrone.com/en-eu/pages/edelkrone-system-wide-features-guide](https://edelkrone.com/en-eu/pages/edelkrone-system-wide-features-guide)

## Equipment 
1. Edelkrone SliderPLUS PRO v5 - [web](https://edelkrone.com/en-eu/products/sliderplus) - [video manual](https://youtube.com/playlist?list=PLbDNm-U3aPIBLN0i_zSxnTywiIBl5LNSk)
2. Edelkrone HeadPLUS v2 - [web](https://edelkrone.com/en-eu/products/headplus) - [video manual](https://youtube.com/playlist?list=PLbDNm-U3aPIB80kGixKJ74C7jljR9x8f3)
3. Vision module - [web](https://edelkrone.com/en-eu/products/vision-module)
4. Focus/Zoom module - [web](https://edelkrone.com/en-eu/products/focus-zoom-module-for-headplus) - [video manual](https://youtube.com/playlist?list=PLbDNm-U3aPIDXNKHIQOUeC5W4sFPyoJ4-)
5. Motor module for SliderPLUS v3- [web](https://edelkrone.com/en-eu/products/motor-module) - [video manual](https://youtube.com/playlist?list=PLbDNm-U3aPIDZlF-qUi4fmElgMzCmoB6x)

## Packing list
1. Misc:
	- Magnetic Snap-On belts for sliderplus
	- Random material
2. Focus/zoom module
3. 3x DC power cable
4. Power sources:
    - 3x DC power adapter 230V-DC jack
    - 230V to 12V D-Tap power source
5. Motor + Lens gear
	- Motor module
	- Set of 4x lens gear ring
6. Misc 2:
    - Controller for edelkrone
    - Shutter release cable
    - NP-F battery bracket for HeadPLUS
7. L-Bracket for HeadPLUS
8. HeadPLUS:
    - Camera mount
    - Laser module
	- 19mm rod for Focus/Zoom module
9. HeadPLUS with mounted Vision module
10. SliderPLUS
### Packing photo
![Packing image](slider.jpg)
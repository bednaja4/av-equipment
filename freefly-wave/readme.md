# Freefly Wave camera
Freefly Wave camera: [https://store.freeflysystems.com/products/wave](https://store.freeflysystems.com/products/wave)

## Equipment 
1. Freefly Wave camera - [web](https://store.freeflysystems.com/products/wave)
2. Irix Cine 45mm T1.5 for Sony E - [web](https://irixlens.com/cine-lenses/cine-lenses-45mm/) 


## Packing list
1. Lens mount shims:
	- 0.5 mm, 0.75 mm, 0.9 mm, 1 mm, 1.1 mm, 1.2 mm
2. Wave Camera
3. Misc:
	- Camera charger/AC adapter
	- D-Tap to Wave power adapter cable
	- 3.0mm hex drive
4. Irix Cine lens
### Packing photo
![Packing image](freefly-wave.jpg)